﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace La_bataille_du_café
{
    public partial class Client : Form
    {
        public Client()
        {
            InitializeComponent();
        }


        private void Client_Load(object sender, EventArgs e)
        {
            byte[] map_trame = new byte[0];
            try
            {
                map_trame = Moteur.Initialisation.GetMap.StartGetMap(); //Récupération de la carte
            }
            catch (System.Net.Sockets.SocketException error)
            {
                MessageBox.Show("La connexion au serveur a échoué !" + "\n" + "Le serveur est injoignable, vérifiez votre connexion internet." + "\n\n" + error, "Erreur de connexion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            Moteur.Initialisation.Decode.StartDecode(map_trame);

            byte nbcaselongueur = 0;
            byte nbcaselargeur = 0;
            int x = 280;
            int y = 132;

            PictureBox start_picture;

            while (nbcaselongueur<10 && nbcaselargeur<10)
            {
                while (nbcaselongueur < 10)
                {
                    start_picture = new PictureBox();
                    Point loc = new Point();
                    loc.X = x;
                    loc.Y = y;

                    start_picture.Name = "pic" + nbcaselongueur.ToString() + nbcaselargeur.ToString();
                    start_picture.Size = new Size(50, 50);
                    start_picture.Location = loc;
                    start_picture.BackColor = Color.White;
                    start_picture.Visible = true;
                    start_picture.BorderStyle = BorderStyle.Fixed3D;
                    start_picture.SizeMode = PictureBoxSizeMode.Zoom;
                    Controls.Add(start_picture);
                    start_picture.BringToFront();
                    x += 50;

                    if (x == (50 * 10 + 280))
                    {
                        x = 280;
                        y += 50;
                    }
                    nbcaselongueur += 1;
                }
                nbcaselargeur += 1;
                nbcaselongueur = 0;
            }

            ShowMap();
        }

        private void ShowMap()
        {
            for (int boucleY = 0; boucleY < 10; boucleY++)
            {
                for (int boucleX = 0; boucleX < 10; boucleX++)
                {
                    if (Moteur.Map.GetUnite(boucleX, boucleY).GetType().Name == "Terre")
                    {
                        foreach(Control ctrlboucle in this.Controls)
                        {
                            if (ctrlboucle.Name == "pic" + boucleX.ToString() + boucleY.ToString())
                            {
                                ctrlboucle.BackColor = Color.DarkOrange;
                            }
                        }
                    }
                    else if (Moteur.Map.GetUnite(boucleX, boucleY).GetType().Name == "Mer")
                    {
                        foreach (Control ctrlboucle in this.Controls)
                        {
                            if (ctrlboucle.Name == "pic" + boucleX.ToString() + boucleY.ToString())
                            {
                                ctrlboucle.BackColor = Color.DarkBlue;
                            }
                        }
                    }
                    else if (Moteur.Map.GetUnite(boucleX, boucleY).GetType().Name == "Foret")
                    {
                        foreach (Control ctrlboucle in this.Controls)
                        {
                            if (ctrlboucle.Name == "pic" + boucleX.ToString() + boucleY.ToString())
                            {
                                ctrlboucle.BackColor = Color.DarkGreen;
                            }
                        }
                    }
                }
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }
    }
}
